artifactory_url="http://101.53.158.194:8081/artifactory"

repo="libs-snapshot-local"

artifacts="com/mkyong"

app_name="spring3helloworld"

url=$artifactory_url/$repo/$artifacts/$app_name
echo $url
file=`curl -s $url/maven-metadata.xml`
echo $file
version=`curl -s $url/maven-metadata.xml | grep latest | sed "s/.*<latest>\([^<]*\)<\/latest>.*/\1/"`
echo $version
build=`curl -s $url/$version/maven-metadata.xml | grep '<value>' |head -1 | sed "s/.*<value>\([^<]*\)<\/value>.*/\1/"`

BUILD_LATEST="$url/$version/$app_name-$build.war"

echo $BUILD_LATEST > filename.txt
